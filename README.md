# AtlassianAppVersions

This Gem obtains info about Atlassian product and plugin releases from the https://marketplace.atlassian.com and https://my.atlassian.com REST APIs.

See also the `atlassian_app_upgradereport` gem which uses this info to produce Confluence reports.

## Usage

There are two top-level classes, `App` instantiated with a product name, and `Plugin` instantiated with a plugin key (key names are from https://marketplace.atlassian.com). `App` and `Plugin` behave similarly, in that they expose a bunch of properties (call the `.keys` method to see what's available), have `version` and `versions` methods to list their versions, and a set of methods (

Sample API use:

```ruby
require 'atlassian_app_versions'
include AtlassianAppVersions

# Info about an Atlassian product

conf = App.new("confluence")
conf.latest		# #<AtlassianAppVersions::LatestAppVersion:70118429916620 5.9.10>
conf.version("5.9.10")		# => #<AtlassianAppVersions::LatestAppVersion:70271671608160 5.9.10>
conf.version("5.9.10").version		# => => "5.9.10"
conf.version("5.9.10").releaseDate	# => #<DateTime: 2016-05-05T00:00:00+00:00 ((2457514j,0s,0n),+0s,2299161j)>
conf.version("5.9.10").relNotes		# => "http://confluence.atlassian.com/display/DOC/Confluence+5.9.10+Release+Notes"


conf.versions("5.9.1", "5.9.5")		#  => [#<AtlassianAppVersions::ArchivedAppVersion:70118438447880 5.9.5>,
 					#<AtlassianAppVersions::ArchivedAppVersion:70118438447860 5.9.4>,
  					#<AtlassianAppVersions::ArchivedAppVersion:70118438447840 5.9.3>,
   					#<AtlassianAppVersions::ArchivedAppVersion:70118438447820 5.9.2>]
conf.latest.relNotes	# => "http://confluence.atlassian.com/display/DOC/Confluence+5.9.10+Release+Notes"
conf.latest.keys	# => ["description", "zipUrl", "tarUrl", "md5", "size", "released", ...]
conf.latest.zipUrl	# => "https://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-5.9.10.tar.gz"


# JQL for bugs fixed in the latest release
conf.bugsJQL(conf.versions[1], conf.versions[0])	# => "issuetype=Bug AND project=CONF AND fixVersion in (5.9.10) AND status in (Resolved, Closed, Soaking) ORDER BY votes DESC, priority DESC, key DESC"


# Info about an Atlassian plugin

plugin = Plugin.new("de.scandio.confluence.plugins.pocketquery")
plugin.name	# => "PocketQuery"
# Available info on the plugin
plugin.keys	# => ["downloadCount", "name", "deployment", "deployable", "lastModified", "description",..]

# Available info a specific release
plugin.latest.keys	# => ["supportType", "releaseDate", "deployment", "deployable", "marketplaceType", "compatibilities", "screenshots", "releasedBy", "license", "version", "links", "status", "marketplaceAgreementAccepted", "pluginSystemVersion", "instructions", "autoUpdateAllowed", "compatibleApplications", "stable", "buildNumber", "summary", "addOnType"]

plugin.latest.version # => "2.0.7"

```


## Installation

- Install Ruby 2.3.1 or above (I use [ruby-install](https://github.com/postmodern/ruby-install) + [chruby](https://github.com/postmodern/chruby)). Then:
```bash
gem install bundler
git clone https://redradish@bitbucket.org/redradish/atlassian_app_versions.git
cd atlassian_app_versions
bundle install	# Install this gem's deps
bundle exec rake install	# Install this gem
bundle exec rake test	# Ensure it's all happy
```
Then try the sample code above in `irb` or `pry`.

## Development

In the finest tradition of Ruby gems, this gem is a hasty hack thrown together. The code would make experienced Rubyists cry. There is no guarantee of API stability. 

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

