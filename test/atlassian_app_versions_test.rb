require 'test_helper'

class AtlassianAppVersionsTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::AtlassianAppVersions::VERSION
  end

  def test_app_instantiation
	  assert_raises RuntimeError do
		  AtlassianAppVersions::App.new("nonexistentproduct")
	  end

	  conf = AtlassianAppVersions::App.new("confluence")
	  refute_nil conf
	  assert_equal("Confluence", conf.name)
	  assert_equal("applications", conf.product_type)
	  refute_nil conf.hostingSupport
	  refute_nil conf.name
	  refute_nil conf.introduction
	  refute_nil conf.pluginCount
	  refute_nil conf.key
	  refute_nil conf.links
	  refute_nil conf.atlassianConnectSupport
	  refute_nil conf.order
	  refute_nil conf.status
	  refute_nil conf.categories
	  assert_raises NoMethodError do
		  conf.nonexistentproperty
	  end
  end 

  def test_plugin_instantiation
	  plugin = AtlassianAppVersions::Plugin.new("de.scandio.confluence.plugins.pocketquery")
	  refute_nil plugin.description
	  refute_nil plugin
	  assert_equal "PocketQuery", plugin.name
	  assert_equal "plugins", plugin.product_type
	  refute_nil plugin.versions.find("2.0.7")

	  refute_nil plugin.downloadCount
	  refute_nil plugin.name
	  refute_nil plugin.deployment
	  refute_nil plugin.deployable
	  refute_nil plugin.lastModified
	  refute_nil plugin.description
	  refute_nil plugin.tagLine
	  refute_nil plugin.dataCenterCompatible
	  refute_nil plugin.links
	  refute_nil plugin.reviewSummary
	  refute_nil plugin.approval
	  refute_nil plugin.pluginKey
	  refute_nil plugin.isOldVersion
	  refute_nil plugin.activeInstalls
	  refute_nil plugin.reviews
	  refute_nil plugin.categories
	  refute_nil plugin.creationDate
	  refute_nil plugin.media
	  refute_nil plugin.vendor
	  refute_nil plugin.compatibleApplications
	  refute_nil plugin.summary
	  assert_raises NoMethodError do
		  plugin.nonexistentproperty
	  end
  end

  def test_jira_bugs_jql
	  jira = AtlassianAppVersions::App.new("jira-core")
	  assert_equal "issuetype=Bug AND project=JRA AND fixVersion in ('7.1.6 Server') AND status in (Resolved, Closed, Soaking, \"Released to Server\") ORDER BY votes DESC, priority DESC, key DESC", jira.bugsJQL("7.1.4", "7.1.6")
	  assert_equal "issuetype=Bug AND project=JRA AND fixVersion in ('7.1.6 Server', 7.1.4) AND status in (Resolved, Closed, Soaking, \"Released to Server\") ORDER BY votes DESC, priority DESC, key DESC", jira.bugsJQL("7.1.2", "7.1.6")
  end

  def test_jira_software_bugs_jql
	  # Verify that 'jira-software' includes JSW bugs
	  jira = AtlassianAppVersions::App.new("jira-software")
	  assert_equal "issuetype=Bug AND project in (JRA,JSW) AND fixVersion in ('7.1.7 Server') AND status in (Resolved, Closed, Soaking, \"Released to Server\") ORDER BY votes DESC, priority DESC, key DESC", jira.bugsJQL("7.1.6", "7.1.7")
  end

  def test_jira_bugs_jql_range_no_upper_bound
	  jira = AtlassianAppVersions::App.new("jira-core")
	  # Verify that JQL for "bugs from 7.1.4 up" includes 7.1.6 Server
	  assert jira.bugsJQL("7.1.4") =~ /7.1.6 Server/
  end

  def test_conf_bugs_jql
	  confluence = AtlassianAppVersions::App.new("confluence")
	  assert_equal "issuetype=Bug AND project in (CONF,CRA) AND fixVersion in (5.9.10, 5.9.9, 5.9.8) AND status in (Resolved, Closed, Soaking, \"Released to Server\") ORDER BY votes DESC, priority DESC, key DESC", confluence.bugsJQL("5.9.7", "5.9.10")
  end

  def test_conf_features_jql
	  confluence = AtlassianAppVersions::App.new("confluence")
	  assert_equal "issuetype!=Bug AND project in (CONF,CRA) AND fixVersion in (5.9.10, 5.9.9, 5.9.8) AND status in (Resolved, Closed, Soaking, \"Released to Server\") ORDER BY votes DESC, priority DESC, key DESC", confluence.featuresJQL("5.9.7", "5.9.10")
  end

  def test_backwards_versions
	  conf = AtlassianAppVersions::App.new("confluence")
	  assert_equal conf.versions("5.9.10", "5.9.7"), conf.versions("5.9.7", "5.9.10")
  end
  def test_recentbugs_jql
	  confluence = AtlassianAppVersions::App.new("confluence")
	  assert_equal "project in (CONF,CRA) AND issuetype=Bug AND affectedVersion in (5.9.10, 5.9.9) AND affectedVersion not in (5.9.8, 5.9.7, 5.9.6, 5.9.5, 5.9.4, 5.9.3, 5.9.2, 5.9.1, 5.8.18, 5.8.17, 5.8.16, 5.8.15, 5.8.14, 5.8.13, 5.8.10, 5.8.9, 5.8.8, 5.8.6, 5.8.5, 5.8.4, 5.8.2, 5.7.6, 5.7.5, 5.7.4, 5.7.3, 5.7.1, 5.7, 5.6.6, 5.6.5, 5.6.4, 5.6.3, 5.6.1, 5.6, 5.5.7, 5.5.6, 5.5.4, 5.5.3, 5.5.2, 5.5.1, 5.5, 5.4.4, 5.4.3, 5.4.2, 5.4.1, 5.4, 5.3.4, 5.3.1, 5.3, 5.2.5, 5.2.4) AND resolution is empty ORDER BY votes DESC, priority DESC, key DESC", confluence.recentbugsJQL("5.9.10", "5.9.8", 50)

	  # Display bugs raised in the 3 versions up to and including 5.9.4, but after 5.9.2 (i.e. 5.9.4 and 5.9.3), ignoring bugs affecting 5.9.2 and earlier.
	  assert_equal "project in (CONF,CRA) AND issuetype=Bug AND affectedVersion in (5.9.4, 5.9.3) AND affectedVersion not in (5.9.2, 5.9.1, 5.8.18, 5.8.17, 5.8.16, 5.8.15, 5.8.14, 5.8.13, 5.8.10, 5.8.9, 5.8.8, 5.8.6, 5.8.5, 5.8.4, 5.8.2, 5.7.6, 5.7.5, 5.7.4, 5.7.3, 5.7.1, 5.7, 5.6.6, 5.6.5, 5.6.4, 5.6.3, 5.6.1, 5.6, 5.5.7, 5.5.6, 5.5.4, 5.5.3, 5.5.2, 5.5.1, 5.5, 5.4.4, 5.4.3, 5.4.2, 5.4.1, 5.4, 5.3.4, 5.3.1, 5.3, 5.2.5, 5.2.4, 5.2.3, 5.2, 5.1.5, 5.1.4, 5.1.3, 5.1.2) AND resolution is empty ORDER BY votes DESC, priority DESC, key DESC", confluence.recentbugsJQL("5.9.2", "5.9.4")
  end

  def test_version_comparisons
	  v0 = AtlassianAppVersions::AbstractVersion.new("7.0.4", nil)
	  v1 = AtlassianAppVersions::AbstractVersion.new("7.1.4", nil)
	  v2 = AtlassianAppVersions::AbstractVersion.new("7.1.6", nil)
	  v3 = AtlassianAppVersions::AbstractVersion.new("7.2", nil)
	  assert v3 > v2
	  assert v2 > v1
	  assert v1 > v0
	  assert v3 > v1
	  assert v3 > v0
	  assert v2 > v0
  end

  def test_releasedate
	  jira = AtlassianAppVersions::App.new("jira-core")
	  assert_equal DateTime.strptime('2016-05-04T00:00:00.000Z', '%Y-%m-%dT%H:%M:%S.%L%z'), jira.version("7.1.6").releaseDate
  end

  def test_app_bugsurl
	  bitbucket = AtlassianAppVersions::App.new("bitbucket")
	  assert_equal "https://jira.atlassian.com/issues/?jql=issuetype%3DBug+AND+project%3DBSERV+AND+fixVersion+in+%284.6.1%29+AND+status+in+%28Resolved%2C+Closed%2C+Soaking%2C+%22Released+to+Server%22%29+ORDER+BY+votes+DESC%2C+priority+DESC%2C+key+DESC", bitbucket.bugsURL("4.6.0", "4.6.1")
  end
end
