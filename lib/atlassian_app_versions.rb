#######
# Wraps the marketplace.atlassian.com and my.atlassian.com REST APIs to obtain info about JIRA releases. See commented-out code near end for examples.
#
# marketplace.atlassian.com data returns simple version data for plugins and apps.
# my.atlassian.com data returns release notes and download locations, but only for apps.
#
# We maintain the facade of having Version objects. Versions get their data by requesting either MPAC or MAC JSON from their @parent, which caches the JSON.
#
require 'open-uri'
require 'open-uri/cached'
require 'json'
require 'cgi'

module AtlassianAppVersions

    # A Plugin or App. This class assumes the MPAC API will have info about the product.
    class AbstractProduct

	JAC_URL="https://jira.atlassian.com"

	attr_accessor :key
	# Initialize with a MPAC key (e.g. "jira" or "com.pyxis.greenhopper.jira")
	def initialize(key)
	    @key=key
	end

        # JQL for all issues resolved after +fromVer+ up to and including +toVer+.
	def allissuesJQL(fromVer=nil, toVer=nil)
	    templateJQL(fromVer, toVer)
	end

        # JQL for all Bugs resolved after +fromVer+ up to and including +toVer+.
	def bugsJQL(fromVer=nil, toVer=nil)
	    "issuetype=Bug AND " + templateJQL(fromVer, toVer)
	end

        # JQL for all security Bugs resolved after +fromVer+ up to and including +toVer+.
	def securityBugsJQL(fromVer=nil, toVer=nil)
	    "issuetype=Bug AND labels=security AND " + templateJQL(fromVer, toVer, "ORDER BY priority desc, votes desc, key desc")
	end

        # JQL for all features resolved after +fromVer+ up to and including +toVer+.
	def featuresJQL(fromVer=nil, toVer=nil)
	    "issuetype!=Bug AND " + templateJQL(fromVer, toVer)
	end

        # JQL for all new, unresolved Bugs _introduced_ in the (up to) 3 versions before +toVer+, but after +fromVer+. This means bugs reported against a version in the range, but not reported against an earlier version (up to +pastVersionsToIgnore+ old versions are considered, since some really old versions are missing from JAC and break the query).
	def recentbugsJQL(fromVer, toVer=nil, includedVersionCount=3, pastVersionsToIgnore=50)
	    new = versions(fromVer, toVer).first(includedVersionCount)
	    old = versions.select { |v| v < new.last }.first(pastVersionsToIgnore)

	    projectJQL(fromVer) + " AND issuetype=Bug AND affectedVersion in (#{versionsListJQL(new)}) AND affectedVersion not in (#{versionsListJQL(old)}) AND resolution is empty ORDER BY votes DESC, priority DESC, key DESC"
	end

        # URL for all issues resolved after +fromVer+ up to and including +toVer+.
	def allissuesURL(fromVer=nil, toVer=nil)
	    JAC_URL + "/issues/?jql=#{CGI::escape(allissuesJQL(fromVer, toVer))}"
	end

        # URL for all Bugs resolved after +fromVer+ up to and including +toVer+.
	def bugsURL(fromVer=nil, toVer=nil)
	    JAC_URL + "/issues/?jql=#{CGI::escape(bugsJQL(fromVer, toVer))}"
	end

        # URL for all features resolved after +fromVer+ up to and including +toVer+.
	def featuresURL(fromVer=nil, toVer=nil)
	    JAC_URL + "/issues/?jql=#{CGI::escape(featuresJQL(fromVer, toVer))}"
	end

        # URL for all new, unresolved Bugs _introduced_ after +fromVer+ up to and including +toVer+. This means bugs reported against a version in the range, but not reported against an earlier version.
	def recentbugsURL(fromVer=nil, toVer=nil)
	    JAC_URL + "/issues/?jql=#{CGI::escape(recentbugsJQL(fromVer, toVer))}"
	end

	# Fetch the versions as they exist on JAC
	def jacVersions
	    if ! @jacVersions then
	    (jacKey.respond_to?(:each) ? "project in (#{jacKey.join(',')})" : "project=#{jacKey}")
		@jacVersions = Array(jacKey).collect { |jacKey|
		    url = JAC_URL + "/rest/api/2/project/#{jacKey}/versions"
		    jsonStr = nil
		    begin
			jsonStr = open(url).read
		    rescue OpenURI::HTTPError => e
			puts "HTTP open of #{url} failed: #{e}" 
			binding.pry
		    end
		    JSON.parse(jsonStr).collect { |v| v["name"] }
		}.flatten.uniq
	    end
	@jacVersions
	end

	def marketplaceJSON
	    if !@marketplaceJSON then
		mpacKey = case key
			  when "jira-core", "jira-software" then
			      # mpac still only knows about 'jira'
			      "jira"
			  when "stash" then "bitbucket" # MPAC actually redirects stash to bitbucket, but be nice and do it here.
			  else
			      key
			  end
		url = "https://marketplace.atlassian.com/rest/1.0/#{product_type}/#{mpacKey}"
		begin
		    jsonStr = open(url).read
		rescue OpenURI::HTTPError => error
		    if error.io.status[0] == "404" then
			raise "No plugin with key '#{key}' found"
		    else
			raise error
		    end
		end
		@marketplaceJSON = JSON.parse( jsonStr )
	    end
	    @marketplaceJSON
	end

	def version_enterprise()
	    enterprise_release_regex = case key
					when "jira-software", "jira-core"
					     /7\.13\..+/
					when "confluence"
					     /6\.13\..+/
					 end
	    allVersions.find { |v| v.version =~ enterprise_release_regex }
	end

	## Find versions in a range, from +fromVer+ (exclusive) to +toVer+ (inclusive). If +toVer+ is omitted, all versions up to the latest are found. If +fromVer+ and +toVer+ are omitted, all versions are returned.
	def versions(fromVer=nil, toVer=nil) # fromVer and toVer may be a string or a *Version
	    if (!fromVer && !toVer) then
		allVersions
	    else
		# allVersions is ordered most to least recent
		# If 'fromVer' was unspecified, count from the end (allVersions.size = last = oldest)
		fromIdx = fromVer ? allVersions.find_index { |v| v.version == fromVer.to_s } || ( raise "Couldn't find #{@key} fromVer #{fromVer}" ) : allVersions.size
		# If toVer was unspecified, count from the beginning (0 = newest)
		toIdx = toVer ? allVersions.find_index{ |v| v.version == toVer.to_s } || ( binding.pry; raise "Couldn't find #{@key} toVer #{toVer}" ) : 0
		# fromIdx may be greater than toIdx, which the slice doesn't like, so get the max/min here
		from = [fromIdx, toIdx].min
		to = [fromIdx, toIdx].max
		allVersions.slice(from, (to - from))
	    end
	end

	## Find a specific version
	def version(ver)
	    allVersions.find { |v| v.version == ver }
	end


	def allVersions
	    raise "Override me!"
	end
	def latest
	    allVersions.first
	end

	# Display all 'properties' of the app/plugin. These can be used as methods, e.g. 'jira.name' or 'plugin.summary'
	def keys
	    marketplaceJSON.keys
	end

	def method_missing(name, *args, &block)
	    if keys and keys.member? name.to_s then
		marketplaceJSON[name.to_s]
	    else
		super
	    end
	end

	def respond_to_missing?(name, include_private = false)
	    keys.member? name.to_s || super
	end

	#private 

	def jacKey
	    case @key
	    when "jira-core" then "JRASERVER"
	    when "jira-software" then ["JRASERVER", "JSWSERVER"]
	    when "confluence" then ["CONFSERVER"]
	    when "crowd" then "CWD"
	    when "gh" then"GHS"
	    when "stash" then "BSERV"
	    when "bitbucket" then "BSERV"
	    when "fisheye" then "FE"
	    when "com.pyxis.greenhopper.jira" then "JSWSERVER"
	    else raise "Unknown product: #{@key}"
	    end
	end

	# "plugins" or "applications"
	def product_type
	    raise "Override me!"
	end


	# Return a 'project in (...)' JQL clause.
	def projectJQL(fromVer=nil, toVer=nil)
	    (jacKey.respond_to?(:each) ? "project in (#{jacKey.join(',')})" : "project=#{jacKey}")
	end

	# Return a comma-separated list of JAC version strings, given an Array of versions
	def versionsListJQL(versions)
	    versions.inject([]) { |all, v|
		all << jacVersions.select { |jacVer| jacVer.start_with? v.version }
	    }.flatten.uniq.collect { |vstr| "'#{vstr}'" }.join(", ")
	end

	# JQL common between bugs and features
	def templateJQL(fromVer=nil, toVer=nil, sort=nil)
	    vers = versionsListJQL(versions(fromVer, toVer))
	    projectJQL(fromVer, toVer) + 
		" AND fixVersion in (#{vers}) AND status in (Resolved, Closed, Soaking, \"Released to Server\") " + (sort ? sort : "ORDER BY votes DESC, priority DESC, key DESC")
	end
    end

    ## Represents a plugin, with information taken from https://marketplace.atlassian.com
    class Plugin < AbstractProduct

	def versionsJSON
	    marketplaceJSON["versions"]["versions"]
	end
	def product_type
	    "plugins"
	end

	def allVersions
	    versionsJSON.collect { |v| PluginVersion.new(v["version"], self) }
	end

	# A few plugins are tracked on JAC, so if asked for JQL, return their issue-tracker link.
	def allissueJQL(fromVer=nil, toVer=nil)
	    links = versionsJSON.first["links"] 
	    if links then
		tracker = links.find { |l| l["rel"] == "issue-tracker" }
		if tracker then
		    if tracker["href"] =~ /jira\.atlassian\.com/ then
			templateJQL(fromVer, toVer)
		    else
			nil
		    end
		end
	    end
	end
    end

    ## Represents an application on jira.atlassian.com / my.atlassian.com
    class App < AbstractProduct
	VALID_PRODUCTS=["jira-core", "jira-software", "confluence", "bitbucket", "stash", "fisheye", "crowd"]
	def initialize(key)
	    if !VALID_PRODUCTS.member? key then
		raise "Unknown product: #{key}. Use one of #{VALID_PRODUCTS.join(", ")}"
	    end
	    if key=="stash" then key="bitbucket"; end
	    super

	end

	def getjson(url)
	    open(url).read.gsub(/^downloads\(/, '').gsub(/\)$/, '')
	end

	def latestReleaseJSON
	    if !@latestReleaseJSON then

		macKey = @key == "bitbucket" ? "stash" : @key # MAC still thinks it's 'stash'
		url="https://my.atlassian.com/download/feeds/current/#{macKey}.json"
		jsonStr = getjson(url)
		@latestReleaseJSON = JSON.parse(jsonStr)
	    end
	    @latestReleaseJSON
	end

	def archivedReleaseJSON
	    if !@archivedReleaseJSON then
		macKey = @key == "bitbucket" ? "stash" : @key # MAC still thinks it's 'stash'
		url="https://my.atlassian.com/download/feeds/archived/#{macKey}.json"
		jsonStr = getjson(url)
		@archivedReleaseJSON = JSON.parse( jsonStr )
	    end
	    @archivedReleaseJSON
	end

	def versionsJSON
	    marketplaceJSON["versions"]
	end

	def allVersions
	    first=true
	    versionsJSON.collect { |v|
		if first then
		    first=false
		    LatestAppVersion.new(v["version"], self)
		else
		    ArchivedAppVersion.new(v["version"], self)
		end
	    }
	end

	def product_type
	    "applications"
	end
    end

    ## A plugin or app version
    class AbstractVersion
	include Comparable

	attr_accessor :version
	def initialize(versionName, parent) 
	    @version = versionName
	    @parent = parent
	end

	def versionJSON
	    @parent.versionsJSON.find { |v| v["version"] == @version }
	end

	def releaseDate
	    DateTime.strptime(versionJSON["releaseDate"], '%Y-%m-%dT%H:%M:%S.%L%z') if versionJSON
	end

	def inspect
	    "#<#{self.class.name}:#{self.object_id} #{to_s}>"
	end

	def to_s
	    @version
	end

	def <=>(other_version)
	    # Confluence has a version with underscores (3.0.0_01) which Gem::Version doesn't like, so strip it
	    v1 = @version.gsub('_', '.')
	    v2 = other_version.to_s.gsub('_', '.')
	    begin
		Gem::Version.new(v1) <=> Gem::Version.new(v2)
	    rescue ArgumentError => e
		$stderr.puts e.message
	    end

	end

	def keys
	    versionJSON.keys
	end

	def method_missing(name, *args, &block)
	    if versionJSON and versionJSON.keys.member? name.to_s then
		versionJSON[name.to_s]
	    else
		super
	    end
	end

	def respond_to_missing?(name, include_private = false)
	    keys.member? name.to_s || super
	end
    end

    class PluginVersion < AbstractVersion
	# Some plugin release don't have release notes, in which case return nil
	def relNotes
	    if versionJSON["releaseNotes"] then
		versionJSON["releaseNotes"].scan(/(?<=href=")[^"]+elease\+[nN]otes/)&.first&.strip
	    end
	end
    end

    class AppVersion < AbstractVersion
	def releaseJSON
	    raise "Override me"
	end

	def relNotes
	    # RC releases don't have release notes, in which case return nil
	    if releaseJSON
		    releaseJSON["releaseNotes"]&.strip
	    end
	end

	def keys
	    # Combine versionJSON keys with releaseJSON keys. Note that Confluence 5.6 and earlier doesn't have releaseJSON.
	    releaseJSON ? releaseJSON.keys + super : super
	end

	def method_missing(name, *args, &block)
	    if releaseJSON and releaseJSON.keys.member? name.to_s then
		releaseJSON[name.to_s]
	    else
		super
	    end
	end

	def respond_to_missing?(name, include_private = false)
	    keys.member? name.to_s || super
	end
    end

    class LatestAppVersion < AppVersion
	def releaseJSON
	    @parent.latestReleaseJSON.select {|e|
		url=e["zipUrl"]
		@parent.key=="fisheye" ||
		    url =~ /.tar.gz$/ && url !~ /(war|cluster).tar.gz$/
	    }[0]
	end
    end
    class ArchivedAppVersion < AppVersion
	# Note: returns nil if that version wasn't available in the archive
	def releaseJSON
	    @parent.archivedReleaseJSON.find { |v| v["version"] == @version }
	end
    end

end
# vim: set smartindent sw=4:
